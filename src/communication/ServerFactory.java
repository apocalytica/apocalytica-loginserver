package communication;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import common.Config;
import common.Loading;
import communication.parsing.DataAnalyzer;
import objects.Client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class ServerFactory {

    private static Server server;
    private static HashMap<Integer, Client> clientList = new HashMap<Integer, Client>();

    public ServerFactory() {

        System.out.println("Blood Of Pain LoginServer");
        System.out.print("Starting SQLFactory : ");
        new SQLFactory();
        new Loading();

        System.out.print("Starting InterServer : ");
        new InterServerFactory();

        try {

            server = new Server();

            server.addListener(new Listener() {
                @Override
                public void received(Connection connection, Object object) {
                    if (object instanceof String) {
                        String packet = (String) object;
                        System.out.println("Recv << " + packet);
                        DataAnalyzer.parse(packet, clientList.get(connection.getID()));
                    }
                }

                @Override
                public void connected(Connection connection) {
                    System.out.println("Connection of " + connection.getRemoteAddressTCP());
                    addClient(new Client(connection));
                }

                @Override
                public void disconnected(Connection connection) {
                    System.out.println("ClientID " + connection.getID() + " disconected");
                    removeClient(clientList.get(connection.getID()));
                }
            });

            server.start();
            server.bind(Config.PORT);

            System.out.println("Server started on port " + Config.PORT + " !");
            System.out.println("Attempt client ...");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Server getServer() {
        return server;
    }

    public static void sendBrodcastPacket(String datas) {
        Iterator<Client> vIter = clientList.values().iterator();

        while (vIter.hasNext()) {
            Client value = vIter.next();

            value.sendPacket(datas);
        }

        System.out.println("Send [Broadcast](TCP) >> " + datas);
    }

    public void addClient(Client client) {
        clientList.put(client.getID(), client);
    }

    public void removeClient(Client client) {
        clientList.remove(client.getID());
    }

    public Client getClient(Client client) {
        return clientList.get(client.getID());
    }

}
