package communication.parsing;

import com.esotericsoftware.kryonet.Connection;
import common.Loading;
import communication.ServerFactory;
import objects.GameServer;

public class InterDataAnalyzer {

    public InterDataAnalyzer() {

    }

    public static void parse(String datas, Connection con) {
        switch (datas.charAt(0)) {
            case 'G': // GameServer
                switch (datas.charAt(1)) {
                    case 'R': // Register
                        String[] gsDatas = datas.substring(2).split(";");
                        GameServer gs = Loading.getGameServers().get(Integer.parseInt(gsDatas[0]));

                        gs.setConnection(con);
                        gs.setIP(con.getRemoteAddressTCP().getHostName());
                        gs.setPort(Integer.parseInt(gsDatas[1]));
                        gs.setOnline(true);

                        System.out.println("GameServer " + gs.getName() + "(" + gs.getID() + ") is registered !");
                        con.sendTCP("GR1");
                        ServerFactory.sendBrodcastPacket("GL" + GameParser.getGameServerList());

                        break;
                }
                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

}
