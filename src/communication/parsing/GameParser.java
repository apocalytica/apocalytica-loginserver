package communication.parsing;

import common.Loading;
import objects.Client;
import objects.GameServer;

import java.util.Iterator;
import java.util.UUID;

public class GameParser {

    public GameParser() {

    }

    public static void parse(String datas, Client client) {
        switch (datas.charAt(1)) {
            case 'L': // List
                client.sendPacket("GL" + getGameServerList());

                break;

            case 'S': // Select
                int gsID = Integer.parseInt(datas.substring(2));
                GameServer gs = Loading.getGameServers().get(gsID);
                String ticket = UUID.randomUUID().toString().replace("-", "").substring(0, 10);
                client.setTicket(ticket);
                gs.sendPacket("Tr" + client.getGameTransfertToGameServer());
                client.sendPacket("Tr" + client.getGameTransfertToClient(gsID));

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

    public static String getGameServerList() {
        Iterator<GameServer> vIter = Loading.getGameServers().values().iterator();
        StringBuilder sBuild = new StringBuilder();

        while (vIter.hasNext()) {
            GameServer value = vIter.next();

            sBuild.append(value.getID() + ";");
            sBuild.append(value.getName() + ";");
            sBuild.append(value.getIP() + ";");
            sBuild.append(value.getPort() + ";");
            sBuild.append(((value.isOnline()) ? "1" : "0") + ((vIter.hasNext()) ? "|" : ""));
        }

        return sBuild.toString();
    }
}
