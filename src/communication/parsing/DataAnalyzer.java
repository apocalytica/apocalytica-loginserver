package communication.parsing;

import objects.Client;

public class DataAnalyzer {

    public DataAnalyzer() {

    }

    public static void parse(String datas, Client client) {
        switch (datas.charAt(0)) {
            case 'A': // Account
                AccountParser.parse(datas, client);

                break;

            case 'G': // Game
                GameParser.parse(datas, client);

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

}
