package communication.parsing;

import communication.SQLFactory;
import objects.Account;
import objects.Client;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountParser {

    public AccountParser() {

    }

    public static void parse(String datas, Client client) {
        switch (datas.charAt(1)) {
            case 'L': // Login
                String[] accDatas = datas.substring(2).split(";");

                ResultSet result = SQLFactory.query("SELECT * FROM accounts WHERE User = '" + accDatas[0] + "'");
                Account acc = null;

                try {
                    if (result.next()) {
                        acc = new Account(
                                result.getLong("GUID"),
                                result.getString("User"),
                                result.getString("Password"),
                                result.getBoolean("Banned")
                        );

                        client.setAccount(acc);
                    } else {
                        client.sendPacket("ALea"); // Error Account
                        client.disconnect();

                        break;
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (acc != null) {
                    if (!acc.validAccount()) {
                        client.sendPacket("ALeb"); // Error Banned
                        client.disconnect();
                    } else if (!acc.getPassword().equals(accDatas[1])) {
                        client.sendPacket("ALep"); // Error Password
                        client.disconnect();
                    } else {
                        client.sendPacket("ALok"); // Can connect
                    }
                }

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown : Kick client !");
                client.disconnect();

                break;
        }
    }

}
