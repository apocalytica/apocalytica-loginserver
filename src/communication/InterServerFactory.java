package communication;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import common.Config;
import common.Utils;
import communication.parsing.GameParser;
import communication.parsing.InterDataAnalyzer;
import objects.Account;
import objects.Client;
import objects.GameServer;

import java.io.IOException;

public class InterServerFactory {

    private Server server;

    public InterServerFactory() {
        try {
            server = new Server();

            server.getKryo().register(Client.class);
            server.getKryo().register(Account.class);
            server.getKryo().register(Connection.class);
            server.getKryo().register(Connection[].class);
            server.getKryo().register(Server.class);

            server.addListener(new Listener() {
                @Override
                public void received(Connection connection, Object object) {
                    if (object instanceof String) {
                        String packet = (String) object;
                        InterDataAnalyzer.parse(packet, connection);
                    }
                }

                @Override
                public void connected(Connection connection) {
                    System.out.println("New GameServer connected !");
                }

                @Override
                public void disconnected(Connection connection) {
                    GameServer gs = GameServer.getGameServerByConnection(connection);
                    gs.setOnline(false);
                    ServerFactory.sendBrodcastPacket("GL" + GameParser.getGameServerList());
                    System.out.println("Connection from the GameServer " + gs.getName() + "(" + gs.getID() + ")" + " is lost !");
                }
            });

            server.start();
            server.bind(Config.IGS_PORT);
        } catch (IOException e) {
            System.out.println(" Failed !");
            e.printStackTrace();
            Utils.onErrorExit();
        }
        System.out.println("Ok !");
    }

}
