package objects;

public class Account {

    private long GUID;
    private String name;
    private String password;
    private boolean banned;

    public Account(long pGUID, String pName, String pPassword, boolean pBanned) {
        this.GUID = pGUID;
        this.name = pName;
        this.password = pPassword;
        this.banned = pBanned;
    }

    public boolean validAccount() {
        if (isBanned()) // Account is banned
        {
            return false;
        } else {
            return true;
        }
    }

    public long getGUID() {
        return GUID;
    }

    public void setGUID(long gUID) {
        GUID = gUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

}
