package objects;

import com.esotericsoftware.kryonet.Connection;
import common.Loading;

import java.util.Iterator;

public class GameServer {

    private int ID;
    private String name;
    private String IP;
    private int port;
    private boolean isOnline;

    private Connection connection;

    public GameServer(int ID, String name, String ip, int port) {
        this.ID = ID;
        this.name = name;
        this.IP = ip;
        this.port = port;
    }

    public static GameServer getGameServerByConnection(Connection con) {
        Iterator<GameServer> vIter = Loading.getGameServers().values().iterator();

        while (vIter.hasNext()) {
            GameServer value = vIter.next();

            if (value.getConnection() == con) {
                return value;
            }
        }

        return null;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String iP) {
        IP = iP;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection con) {
        this.connection = con;
    }

    public void sendPacket(Object datas) {
        if (connection != null && connection.isConnected()) {
            connection.sendTCP(datas);
        }
    }

}
