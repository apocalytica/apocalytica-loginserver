package objects;

import com.esotericsoftware.kryonet.Connection;

public class Client {

    private int ID;
    private Connection connection;
    private Account account;
    private String ticket;

    public Client(Connection con) {
        this.ID = con.getID();
        this.connection = con;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public void sendPacket(String packet) {
        System.out.println("Send (TCP) >> " + packet);
        this.connection.sendTCP(packet);
    }

    public void sendFasterPacket(String packet) {
        System.out.println("Send (UDP) >> " + packet);
        this.connection.sendUDP(packet);
    }

    public void disconnect() {
        this.connection.close();
    }

    public String getGameTransfertToClient(int pGsID) {
        StringBuilder sBuild = new StringBuilder();

        sBuild.append(account.getGUID() + ";");
        sBuild.append(ticket + ";");
        sBuild.append(pGsID);

        return sBuild.toString();
    }

    public String getGameTransfertToGameServer() {
        StringBuilder sBuild = new StringBuilder();

        sBuild.append(account.getGUID() + ";");
        sBuild.append(ticket);

        return sBuild.toString();
    }

}
