package common;

import com.esotericsoftware.minlog.Log;
import communication.ServerFactory;

public class Main {

    public static void main(String[] args) {
        Log.set(Log.LEVEL_NONE);
        new ServerFactory();
    }

}
