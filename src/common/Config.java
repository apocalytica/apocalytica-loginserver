package common;

public class Config {

    // Server bind
    //public static final String HOST = "*";
    public static final int PORT = 9902;

    // InterGameServer bind
    public static final int IGS_PORT = 9904;

    // Database
    public static final String DB_HOST = "localhost";
    public static final String DB_USER = "root";
    public static final String DB_PASS = "";
    public static final int DB_PORT = 3306;
    public static final String DB_NAME = "apocalytica_loginserver";

    // Miscs
    private static boolean DEBUG = true;

    public Config() {

    }

}
