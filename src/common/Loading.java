package common;

import communication.SQLFactory;
import objects.GameServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Loading {

    private static HashMap<Integer, GameServer> gameServers = new HashMap<Integer, GameServer>();

    public Loading() {
        this.loadGameServers();
    }

    public static HashMap<Integer, GameServer> getGameServers() {
        return gameServers;
    }

    private void loadGameServers() {
        ResultSet result = SQLFactory.query("SELECT * FROM game_servers");
        int i = 0;

        try {
            while (result.next()) {
                i++;
                gameServers.put(result.getInt("ID"),
                        new GameServer(
                                result.getInt("ID"),
                                result.getString("Name"),
                                null,
                                0
                        ));
            }

            System.out.println("Registered " + i + " gameservers");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
