package common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Utils {

    public Utils() {

    }

    public static void onErrorExit() {
        System.out.println("Press any key to continue ...");
        BufferedReader buffReader = new BufferedReader(new InputStreamReader(System.in));
        String anykey = null;
        try {
            anykey = buffReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.exit(1);
    }

}
